<?php namespace Gentlefox\Laratables;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Afflicto\HTML\HTML;
use Afflicto\HTML\Element;

class Laratable {

	protected static $transformers = [];

	protected static $filterClasses = [];

	/**
	 * The Request
	 * @var \Illuminate\Routing\Request
	 */
	private $request;

	/**
	 * The eloquent query instance.
	 * @var \Illuminate\Database\Eloquent\Builder
	 */
	private $query;

	/**
	 * The raw columns array, as it was passed in to the constructor.
	 * @var [type]
	 */
	private $columnsRaw = [];

	/**
	 * Column configuration array.
	 * @var Array
	 */
	private $columns = [];

	private $defaultTransformers = [];

	/**
	 * The paginator instance.
	 * @var \Illuminate\Pagination\Paginator
	 */
	public $paginator;

	/**
	 * The HTLML table element.
	 * @var \Afflicto\HTML\Element
	 */
	private $table;

	/**
	 * Whether to enable select functionality
	 * @var boolean
	 */
	private $selectable = false;

	/**
	 * The checkbox in the header to "select all"
	 * @var \Afflicto\HTML\Element
	 */
	public $selectableHeader;

	/**
	 * Whether to render the editable action.
	 * @var boolean
	 */
	private $editable = false;

	/**
	 * The URL for editing the model. E.G http://mysite.com/posts/edit/{slug}
	 * single curly brace syntax is used to reference properties on the model.
	 * @var null
	 */
	private $editableURL = null;

	/**
	 * Whether to render the destroyable action.
	 * @var boolean
	 */
	private $destroyable = false;

	/**
	 * URL to the destroy action.
	 * @var null
	 */
	private $destroyableURL = null;

	private $pagination = false;

	private $paginationPerPage = 15;

	/**
	 * Whether to enable sorting functionality.
	 * @var boolean
	 */
	private $sortable = false;

	/**
	 * The columns that the table can be sorted by. Specify the columns as they appear in the database (or on the model).
	 * @var array
	 */
	public $sortableColumns = [];

	/**
	 * The default column to sort by if nothing is specified in the URL
	 *
	 * @var string|null
	 */
	public $sortableDefaultColumn = null;

	/**
	 * The default sort direction if nothing is specified in the URL.
	 *
	 * @var string
	 */
	public $sortableDefaultDirection = 'asc';

	/**
	 * Whether to enable filtering.
	 * @var bool
	 */
	public $filterable = false;

	/**
	 * Contains an array of filters to be applied per column.
	 * @var array ['name' => [LaratableFilter, LaratableFilter...]]
	 */
	public $filters = [];

	/**
	 * Extra URL query parameters to be appended to URL's (pagination & sort)
	 * @var array
	 */
	public $queryParams = [];

	/**
	 * Create a new Laratable.
	 * @param Builder|null
	 * @param Array the columns to display, keys are human-readable names and values are valid table columns.
	 */
	public function __construct(Request $request, Builder $query = null, $columns = []) {
		$this->request = $request;

		# create the table
		$this->table = HTML::table();

		$this->selectableHeader = new Element('input', false, null, [
			'type' => 'checkbox',
			'name' => 'laratable-select-all',
		]);

		# add html classes
		$this->table->addClass(config('laratables.html.classes'));

		# set default transformers
		$this->defaultTransformers = $this->parseTransformerString(config('laratables.transformers', ''));

		# set the query and the columns
		$this->setQuery($query);

		# set pagination config
		if (config('laratables.pagination.enabled', false)) {
			$this->paginate(true, config('laratables.pagination.perPage'));
		}

		# set defaults for editable, destroyable and selectable.
		$this->editable = config('laratables.editable', false);
		$this->destroyable = config('laratables.destroyable', false);
		$this->selectable = config('laratables.selectable', false);
		$this->sortable = config('laratables.sortable', false);

		# set columns
		$this->setColumns($columns);
	}

	public static function registerTransformer($name, $callable) {
		static::$transformers[$name] = $callable;
	}

	public static function registerFilter($name, $class) {
		static::$filterClasses[$name] = $class;
	}

	public function selectable($selectable) {
		$this->selectable = $selectable;
	}

	public function destroyable($destroyable, $url) {
		$this->destroyable = $destroyable;
		$this->destroyableURL = $url;
	}

	public function editable($editable, $url) {
		$this->editable = $editable;
		$this->editableURL = $url;
	}

	/**
	 * @param boolean $sortable
	 * @param array $sortableColumns
	 * @param string|null $defaultColumn
	 * @param string $defaultDirection
	 */
	public function sortable($sortable, $sortableColumns = [], $defaultColumn = null, $defaultDirection = 'asc') {
		$this->sortable = $sortable;
		$this->sortableColumns = $sortableColumns;
		$this->sortableDefaultColumn = $defaultColumn;
		$this->sortableDefaultDirection = $defaultDirection;
	}

	/**
	 * Add a new filter for the given column.
	 *
	 * @param $column
	 * @param $filter
	 * @return mixed
	 */
	public function addFilter($column, $type) {
		# get filter class
		$class = static::$filterClasses[$type];
		$instance = new $class($this->request, $this->getColumnByMachine($column));

		$this->filters[] = [
			'type' => $type,
			'column' => $column,
			'instance' => $instance,
		];

		return $instance;
	}

	/**
	 * Enable or disable filtering.
	 * @param bool|false $filterable
	 */
	public function filterable($filterable = false) {
		$this->filterable = $filterable;
	}

	public function getTransformer($name) {
		return static::$transformers[$name];
	}

	public function setQuery(Builder $query) {
		$this->query = $query;
	}

	public function setColumns($columns) {
		$this->columnsRaw = $columns;
		$this->columns = [];
		foreach($columns as $human => $machine) {
			$relation = null;
			if (is_array($machine)) {
				$transformers = [['name' => $machine[1], 'params' => []]];
				$machine = $machine[0];
				
				//parse column->related syntax
				$relation = null;
                if (preg_match('/[a-z_0-9]+->[a-z_0-9]+/', $machine)) {
                    $rel = explode('->', $machine);
                    $relation = ['related' => $rel[0], 'column' => $rel[1]];
                }
			}else {
				$machine = explode(' ', $machine);
				if (count($machine) > 1) {
					$transformers = $this->parseTransformerString($machine[1]);
					$machine = $machine[0];
				}else {
					$transformers = [];
					$machine = $machine[0];
				}

				//parse column->related syntax
				$relation = null;
				if (preg_match('/[a-z_0-9]+->[a-z_0-9]+/', $machine)) {
					$rel = explode('->', $machine);
					$relation = ['related' => $rel[0], 'column' => $rel[1]];
				}
			}

			$this->columns[$human] = [
				'human' => $human,
				'machine' => $machine,
				'transformers' => $transformers,
				'relation' => $relation
			];
		}
	}

	/**
	 * Parses a transformer string, e.g "escape|limit:50"
	 * @param  [type]
	 * @return array [type]
	 */
	public function parseTransformerString($string) {
		$transformers = [];

		foreach(explode('|', $string) as $tf) {	
			if (mb_strlen($tf) <= 0) continue;
			if (preg_match('/[^:]+:[^:]+/', $tf)) {
				$tf = explode(':', $tf);
				$transformers[] = [
					'name' => $tf[0],
					'params' => explode(',', $tf[1]),
				];
			}else {
				$transformers[] = ['name' => $tf, 'params' => []];
			}
		}

		return $transformers;
	}

	public function appendQueryParams($params) {
		foreach($params as $k => $v) {
			$this->queryParams[$k] = $v;
		}
	}

	public function paginate($pagination = false, $perPage = 15) {
		$this->pagination = $pagination;
		$this->paginationPerPage = $perPage;
		return $this;
	}

	public function getColumnByMachine($machine) {
		foreach($this->columns as $column) {
			if ($column['machine'] == $machine) return $column;
		}

		return null;
	}

	/**
	 * Render a column value.
	 * @param  Model the model instance.
	 * @param  string|callable The database column to render.
	 * @return string|mixed Usually, a string.
	 */
	public function getColumnValue($model, $column) {
		# Do we want the value of a related model?
		# relation->property syntax?
		if (isset($column['relation'])) {
			$related = $model->{$column['relation']['related']};
			if ($related) {
				$value = $related->{$column['relation']['column']};
			}
		}

		if (!isset($value)) {
			if (isset($column['machine'])) {
				$value = $model->{$column['machine']};
			}else {
				$value = '';
			}
		}

		# using custom transformers or default?
		$transformers = $column['transformers'];
		if (empty($transformers)) {
			$transformers = $this->defaultTransformers;
		}

		# pipe the value through the transformers
		foreach($transformers as $transformer) {
			$callable = $transformer['name'];
			$params = array_merge([$model, $column, $value], $transformer['params']);

			if (is_string($callable)) {
				$callable = $this->getTransformer($callable);
			}

			$value = call_user_func_array($callable, $params);
		}

		return '' .$value;
	}

	public function createURL()
	{
		//$dir = ($this->request->query('dir', 'asc') == 'asc') ? 'desc' : 'asc';
		# get sort column and dir
		$sortColumn = $this->request->query('sort', $this->sortableDefaultColumn);
		$sortDir = $this->request->query('dir', $this->sortableDefaultDirection);

		# generate a URL to the current page with sort & dir
		$url = $this->request->url() .'?page=' .$this->request->query('page', 1) .'&sort=' .$sortColumn .'&dir=' .$sortDir;

		# add any filters currently used
		if ($this->filterable) {
			foreach($this->filters as $filter) {
				$filter = $filter['instance'];
				$url .= '&filter_' .$filter->getColumn()['machine'] .'=' .$filter->getValue();
			}
		}

		# add extra query params if needed
		foreach($this->queryParams as $key => $value) {
			$url .= '&' .$key .'=' .$value;
		}

		return $url;
	}

	public function createSortableLink($column) {
		$dir = ($this->request->query('dir', 'asc') == 'asc') ? 'desc' : 'asc';
		$url = $this->request->url() .'?page=' .$this->request->query('page', 1) .'&sort=' .$column .'&dir=' .$dir;

		if ($this->filterable) {
			foreach($this->filters as $filter) {
				$filter = $filter['instance'];
				$url .= '&filter_' .$filter->getColumn()['machine'] .'=' .$filter->getValue();
			}
		}

		foreach($this->queryParams as $key => $value) {
			$url .= '&' .$key .'=' .$value;
		}

		return $url;
	}

	public function createSortableButton($column) {
		$url = $this->createSortableLink($column);
		$dir = $this->request->query('dir', 'asc');
		$active = ($this->request->query('sort', '') == $column) ? true : false;
		return view('laratables::sort', ['direction' => $dir, 'url' => $url, 'active' => $active])->render();
	}

	/**
	 * Build the "thead" HTML Element.
	 * @return Afflicto\HTML\Element the thead element.
	 */
	public function buildHeader() {
		$thead = HTML::thead();
		$tr = HTML::tr();

		# selectable?
		if ($this->selectable) {
			$tr->add(HTML::th($this->selectableHeader));
		}

		# the headings
		foreach($this->columns as $column) {
			$th = HTML::th('' .$column['human']);

			# sortable?
			if ($this->sortable) {
				if (in_array($column['machine'], $this->sortableColumns)) {
					$th->add($this->createSortableButton($column['machine']));
				}
			}

			$tr->add($th);
		}

		# any actions?
		if ($this->destroyable || $this->editable)
			$tr->add(HTML::th());

		$thead->add($tr);

		return $thead;
	}

	public function buildBody() {
		$tbody = HTML::tbody();

		# filter?
		if ($this->filterable) {
			foreach($this->filters as $filter) {
				$filter['instance']->apply($this->query);
			}
		}

		# sort?
		if ($this->sortable) {
			$col = $this->request->query('sort', $this->sortableDefaultColumn);
			$dir = $this->request->query('dir', $this->sortableDefaultDirection);
			if ( ! in_array($dir, ['asc', 'desc'])) {
				$dir = 'asc';
			}

			if (in_array($col, $this->sortableColumns)) {
				//sorting a relationship?
				$column = $this->getColumnByMachine($col);
				if (isset($column['relation'])) {
					$col = $column['relation']['related'] .'_id';
				}
				$this->query->orderBy($col, $dir);
			}
		}

		# paginate ?
		if ($this->pagination) {
			$this->paginator = $this->query->paginate($this->paginationPerPage);
			$this->paginator->appends([
				'sort' => $this->request->query('sort', ''),
				'dir' => $this->request->query('dir', 'asc')
			]);

			$this->paginator->appends($this->queryParams);

			if ($this->filterable) {
				foreach($this->filters as $filter) {
					$filter = $filter['instance'];
					$this->paginator->appends('filter_' .$filter->getColumn()['machine'], $filter->getValue());
				}
			}
		}

		$collection = $this->query->get();
		foreach($collection as $model) {
			$row = HTML::tr();

			# select checkbox
			if ($this->selectable) {
				$td = new Element('input', false, null, [
					'type' => 'checkbox',
					'name' => $this->query->getModel()->getTable() .'[]',
					'value' => $model->id,
					'class' => 'laratable-select-row',
				]);
				$row->add(HTML::td($td));
			}

			# model data
			foreach($this->columns as $human => $machine) {
				$td = HTML::td();

				$td->content = '' .$this->getColumnValue($model, $machine);

				$row->add($td);
			}

			# actions
			if ($this->editable || $this->destroyable) {
				# parse URLS
				if (preg_match('/{([a-z0-9_-]+)}/', $this->editableURL, $matches)) {
					$editableURL = str_replace($matches[0], $model->{$matches[1]}, $this->editableURL);
				}

				if (preg_match('/{([a-z0-9_-]+)}/', $this->destroyableURL, $matches)) {
					$destroyableURL = str_replace($matches[0], $model->{$matches[1]}, $this->destroyableURL);
				}

				# render the actions view
				$actions = view('laratables::actions', [
					'editable' => $this->editable,
					'destroyable' => $this->destroyable,
					'editableURL' => $editableURL,
					'destroyableURL' => $destroyableURL,
				]);

				$row->add(HTML::td($actions->render()));
			}

			$tbody->{$model->id} = $row;
		}

		return $tbody;
	}

	public function buildFooter() {
		$tfoot = HTML::tfoot();

		return $tfoot;
	}

	public function buildFilters() {
		//build filters div
		$filtersDiv = HTML::div()->addClass('filters');

		foreach($this->filters as $filter) {
			$instance = $filter['instance'];
			$filterDiv = HTML::div($instance->buildUI())->addClass('filter');

			$filtersDiv->add($filterDiv);
		}

		//build filter controls div (submit, reset buttons)
		$controlsDiv = HTML::div()->addClass('controls');
		$controlsDiv->submit = HTML::input([], [
			'type' => 'submit',
			'value' => 'Filter',
		]);

		$container = HTML::form([], ['method' => 'GET', 'name' => 'filters'])->addClass('laratable-filters');

		// add page, sort, sort direction and extra queryParams to the form as hidden data.
		$container->add(HTML::input([], ['type' => 'hidden', 'name' => 'page', 'value' => $this->request->query('page', 1)]));
		$container->add(HTML::input([], ['type' => 'hidden', 'name' => 'sort', 'value' => $this->request->query('sort', $this->sortableDefaultColumn)]));
		$container->add(HTML::input([], ['type' => 'hidden', 'name' => 'dir', 'value' => $this->request->query('dir', $this->sortableDefaultDirection)]));

		foreach($this->queryParams as $key => $value) {
			$container->add(HTML::input([], ['type' => 'hidden', 'name' => $key, 'value' => $value]));
		}

		$container->add(HTML::input([], [
			'type' => 'hidden',
			'name' => 'sort',
			'value' => $this->request->query('sort', ''),
		]));

		$container->add(HTML::input([], [
			'type' => 'hidden',
			'name' => 'dir',
			'value' => $this->request->query('dir', 'asc'),
		]));

		$container->filters = $filtersDiv;
		$container->controls = $controlsDiv;
		return $container;
	}

	/**
	 * Builds the header, footer and body and adds them to the table.
	 * @see the $table property.
	 * @return void
	 */
	public function build() {
		$this->table->thead = $this->buildHeader();
		$this->table->tfoot = $this->buildFooter();
		$this->table->tbody = $this->buildBody();
	}

	/**
	 * Render the table. This will call the build method if the tbody property of the table is null.
	 * @return string
	 */
	public function render() {
		# build if we need to
		if (!isset($this->table->tbody)) $this->build();

		# render
		return $this->table->display();
	}

	/**
	 * Renders the table.
	 * @see render
	 * @return string
	 */
	public function __toString() {
		return $this->render();
	}

}
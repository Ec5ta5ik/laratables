<?php namespace Gentlefox\Laratables;

use Illuminate\Support\ServiceProvider;

class LaratableServiceProvider extends ServiceProvider {

	public function boot() {
		$this->publishes([
			__DIR__ .'/config/laratables.php' => config_path('laratables.php'),
		]);

		$this->loadViewsFrom(__DIR__.'/views', 'laratables');
	}

	public function register() {
		$this->app->singleton('laratable', function($app) {
			return new Factory($app['request']);
		});
	}
	
}
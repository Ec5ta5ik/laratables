<?php namespace Gentlefox\Laratables\Filters;

/**
 * A filter which operates on a boolean (true or false, yes or no.)
 */
class BooleanFilter extends Filter {

	protected $defaultValue = 'any';

	protected $values = [];

	protected $allowAny = true;

	protected $trueLabel = 'Yes';
	
	protected $falseLabel = 'No';

	protected $anyLabel = 'Any';
	
	/**
	 * @param  \Illuminate\Database\Query\Builder $query the database query.
	 * @return void
	 */
	public function apply(\Illuminate\Database\Eloquent\Builder $query)
	{
		if ($this->value != 'any') {

			if (isset($this->filterFunction)) {
				return $this->applyFilterFunction($query);
			}

			$column = $this->column['machine'];
			if ($this->column['relation'] != null) {
				$column = $column .'_id';
			}

			$val = (int) $this->value;

			$query->where($column, '=', $val);
		}
	}

	public function allowAny($bool = true) {
		$this->allowAny = $bool;
		$this->defaultValue = $bool ? 'any' : 'no';
	}

	public function setLabels($trueLabel = 'Yes', $falseLabel = 'No', $anyLabel = 'Any')
	{
		$this->trueLabel = $trueLabel;
		$this->falseLabel = $falseLabel;
		$this->anyLabel = $anyLabel;
	}

	private function buildOption($value, $label, $selected = false) {
		$str = '<option ';
		if ($selected) {
			$str .= 'selected ';
		}

		$str .= 'value="' .$value .'">' .$label .'</option>';

		return $str;
	}

	public function buildUI() {
		$str = '<label for="filter_' .$this->column['machine'] .'">' .$this->label .'</label>';

		$str .= '<select name="filter_' .$this->column['machine'] .'">';

		# allow selecting 'any'=
		if ($this->allowAny) {
			$str .= $this->buildOption('any', $this->anyLabel, $this->value == 'any');
		}

		# false
		$str .= $this->buildOption('0', $this->falseLabel, $this->value == '0');

		# true
		$str .= $this->buildOption('1', $this->trueLabel, $this->value == '1');

		$str .= '</select>';

		return $str;
	}

}
<?php namespace Gentlefox\Laratables\Filters;

use Illuminate\Http\Request;

class SearchFilter extends Filter {

	protected $defaultValue = null;

	protected $fuzzy = true;

	public function apply(\Illuminate\Database\Eloquent\Builder $query) {
		if ( ! $this->value) return;

		$column = $this->column['machine'];
		if ($this->column['relation'] != null) {
			$column = $column .'_id';
		}

		if ($this->fuzzy)
			$query->where($column, 'LIKE', '%' .$this->value .'%');
		else
			$query->where($column, '=', $this->value);
	}

	public function fuzzy($boolean) {
		$this->fuzzy = $boolean;
		return $this;
	}

	public function buildUI() {
		$value = ($this->value) ? $this->value : '';

		return '
		<label for="filter_' .$this->column['machine'] .'">' .$this->label .'</label>
		<input type="search" name="filter_' .$this->column['machine'] .'" value="' .$value .'">
		';
	}
}
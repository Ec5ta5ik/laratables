<?php namespace Gentlefox\Laratables;

interface ValueTransformerInterface {

	public function transform($model, $column, $value);

}
@if($editable)
	<a class="btn btn-primary" href="{{$editableURL}}">Edit</a>
@endif

@if($destroyable)
	<form action="{{$destroyableURL}}" method="POST">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="submit" value="Delete" class="btn btn-warning">
	</form>
@endif